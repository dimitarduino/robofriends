import React from 'react';

const Scroll = (props) => {
    return (
        <div style={{overflowY: 'scroll', height: '600px', borderTop: '5px solid #000'}}>
            {props.children}
        </div>
    );
}

export default Scroll;